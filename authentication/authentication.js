const getSessionKey = (z, bundle) => {

    const promise = z.request({
        method: 'POST',
        url: 'http://test-apis.keito.works/user/sign_in',
        body: {
            email: bundle.authData.email,
            password: bundle.authData.password,
            device: 'Zapier'
        }
    })

    

    return promise.then((response) => {
        if (response.status === 400) {
            throw new Error('The username/password you supplied is invalid');
        }
        return {
            sessionKey: JSON.parse(response.content)['AUTH-TOKEN']
        };
    })
}
    
    
module.exports =  {
    type: 'session',
    test: (z,bundle) => {
        const promise = z.request('http://test-apis.keito.works/user');
        return promise.then((response) => {
            if(response.status !== 200)
                throw new Error('Invalid API Key');
        });
    },
    fields: [
        {key: 'email', label: 'Email', 'required': true, type: 'string'},
        {key: 'password', label: 'Password', 'required': true, type: 'password'}
    ],
    sessionConfig: {
        perform: getSessionKey
    }
}