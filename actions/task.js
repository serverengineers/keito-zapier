
module.exports = {
  key: 'task',

  //A noun which will display at zapier action screen
  noun: 'Task',

  display: {
    label: 'Create Task',
    description: 'Creates a new task.'
  },

  //Operation which is to be perfomed for action in Zapier
  operation: {
    inputFields: [
      {key: 'id', required: true, type: 'string', label: 'Id', helpText: 'Reference Id for Keito Task'},
      {key: 'title', required: true, type: 'text', label: 'Title', helpText: 'Title of the task in Keito'},
      {key: 'dueDate', required: true, type: 'text', label: 'Due Date', helpText: 'Due date of task in Keito'},
      {key: 'participants', required: false, type: 'text', label: 'Participants', helpText: 'Participants on task'},
    ],

    //Perform key which will make request for action and return output
    perform: (z, bundle) => {
      
        z.console.log('Printing bundle in actions-->', JSON.stringify(bundle));

        var zonedDateTime = require('js-joda').ZonedDateTime; 

        var dueDate = zonedDateTime.parse(bundle.inputData.dueDate)
        var dueDateInUtC = (dueDate.toEpochSecond()+(dueDate._offset._totalSeconds)) * 1000;
        var currentDateInUtc = new Date().getTime() + (dueDate._offset._totalSeconds*1000);

        if(bundle.inputData.title && bundle.inputData.title.includes('| Keito'))
        {
            return {};
        }

        //Input data for request from Keito Server
        var inputData = {
          title: bundle.inputData.title,
          reference: true,
          channel: 'Zapier',
          channelId: bundle.inputData.id,
          taskState: "Open",
          repeatedTask: false,
          timeStamp: currentDateInUtc,
          dueDate: dueDateInUtC,
          priority: 0
        }

        //If there are some attendents on task
        if(bundle.inputData.participants)
        {
          var participantsList = bundle.inputData.participants.split(",");
          inputData.participantsList = participantsList;
        }

        //sending request to server
        const promise = z.request({
          url: 'http://test-apis.keito.works/task',
          method: 'POST',
          body: JSON.stringify(inputData),
          headers: {
            'content-type': 'application/json; charset=utf-9',
            'AUTH-TOKEN': bundle.authData.sessionKey
          }
        });
        
        return promise.then((response) => JSON.parse(response.content));
    }



  }


}