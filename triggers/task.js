const getTask = (z, bundle) => {
  
    //Request from pool API in Keito
    const requestOptions = {
      url: 'http://test-apis.keito.works/task/tasks_in_pool',
      method: 'GET',
      headers: {
        'content-type': 'application/json; charset=utf-9',
        'AUTH-TOKEN': bundle.authData.sessionKey
      }
    };
    

    // You may return a promise or a normal data structure from any perform method.
    return z.request(requestOptions)
      .then((response) => {
        var items = JSON.parse(response.content);
        z.console.log('Priting response--->', items)
        items.forEach(item => {
          item.originalId = item.id + '-' + item.lastUpdateAt;
          item.id = item.id;
        });

        z.console.log('After processing--> ', items);
        return items;
      });
  };
  
  module.exports = {
    key: 'task',
  
    //Noun for displaying in UX of Zapier
    noun: 'Task',
    display: {
      label: 'New Task',
      description: 'Trigger when a new task is added'
    },
  
    //Buisness Logic
    operation: {
      
      perform: getTask,

      sample: {
        id: "5a61bafe08b4d742fb54f716",
        title: 'Calling Home',
        participantsList: ''
      },

      outputFields: [
        {key: 'id', label: 'Task Id'},
        {key: 'title', label: 'Title'},
        {key: 'participantsList', label: 'Task Participants List'},
        {key: 'channel', label: 'Channel'}
      ]
    }
  
  };