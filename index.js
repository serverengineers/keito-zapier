// We can roll up all our behaviors in an App.

const taskAction = require('./actions/task')

const taskTrigger = require('./triggers/task');

const authentication = require('./authentication/authentication');

const includeSessionKeyHeader = (request, z, bundle) => {

    if(bundle.authData.sessionKey)
    {
      request.headers["user-agent"] = "Zapier"
      request.headers["content-type"] = "application/json; charset=utf-8"
      request.headers["AUTH-TOKEN"] = bundle.authData.sessionKey
    }
    return request;
  }

const App = {
    // This is just shorthand to reference the installed dependencies you have. Zapier will
    // need to know these before we can upload
    version: require('./package.json').version,
    platformVersion: require('zapier-platform-core').version,

    authentication: authentication,
    // beforeRequest & afterResponse are optional hooks into the provided HTTP client
    beforeRequest: [
      includeSessionKeyHeader
    ],

    afterResponse: [
    ],

    // If you want to define optional resources to simplify creation of triggers, searches, creates - do that here!
    resources: {
    },

    // If you want your trigger to show up, you better include it here!
    triggers: {
       [taskTrigger.key]: taskTrigger
    },

    // If you want your searches to show up, you better include it here!
    searches: {
    },

    // If you want your creates to show up, you better include it here!
    creates: {
      [taskAction.key]: taskAction
    }
  };

// Finally, export the app.
module.exports = App;
