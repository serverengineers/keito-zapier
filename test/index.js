const should = require('should');

const zapier = require('zapier-platform-core');

const App = require('../index');
const appTester = zapier.createAppTester(App);

describe('My App', () => {

  it('should load recipes', (done) => {
    bundle = {
      inputData: {
        title: 'New Task In server at 11:21 | Keito',
        id: 1234,
        dueDate: '2018-01-19T11:22:00+05:30',
        participants: 'ykc1608@gmail.com'
      },
      authData: {
        sessionKey: 'eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJodHRwczovL2lkZW50aXR5dG9vbGtpdC5nb29nbGVhcGlzLmNvbS9nb29nbGUuaWRlbnRpdHkuaWRlbnRpdHl0b29sa2l0LnYxLklkZW50aXR5VG9vbGtpdCIsImV4cCI6MTUxNTA0OTI2MCwiaWF0IjoxNTE1MDQ1NjYwLCJpc3MiOiJmaXJlYmFzZS1hZG1pbnNkay1veTloMEBrZWl0by1kZXYuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJzdWIiOiJmaXJlYmFzZS1hZG1pbnNkay1veTloMEBrZWl0by1kZXYuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJ1aWQiOiI1YTJlNDY1ZTA4YjRkNzM3MTA0YmVhZWQifQ.eqKpEAORn2Xs5razzqx1C33vZaDxTI5DnQIzO-NaJ5g5579f0GRbLEibFVHM14ca4SvRQw9Vw-hmFQPLA_Y68vv-yUXgnE7O1BMvaUNqD3N4dqMTg-pMHkgx96HCTpHt4WTsiTE4zkVk_NqhQa2w8ex5Yl8rVnzYnety-2PUieJK2RPt3o4vbMSAJpqoDnpwJKqc4GjkjHu4tCb4bf0JPWznZcA02O63z6n_TLr9g-btFZhSrObJAy3EddRRMWXnJPoBDJwkPaNxpqiKvS0VYfNclRXw4ATwDMy7UgjolDmHwh7jzXPvzyWH1GlSikRKJcGJ9OUo8wByIxx9tDPMZw'
      }
    }

    appTester(App.triggers.task.operation.perform, bundle)
      .then(results => {
        done();
      })
      .catch(done=>console.log(done));
   
  });

});
